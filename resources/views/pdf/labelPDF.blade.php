<!DOCTYPE html>
<html>

<head>
    <title></title>
    <style>
        .page-content {
            max-height: 130mm;
        }

        body {
            font-size: 12px;
        }

        table {
            table-layout: fixed;
            width: 100%;
            border: 1px solid black;
            border-collapse: collapse;
            padding: 4px;
        }

        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 4px;
            text-align: center;
        }

        th {
            width: 30%;
        }

        td {
            width: 35%;
            height: 3%;
        }

        .page-break {
            page-break-after: always;
        }

        /* Estilos para el encabezado */
        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .header img {
            width: 100px;
            /* Ajusta el tamaño de la imagen */
            height: auto;
            /* Mantener la proporción de la imagen */
        }

        .header-text {
            text-align: right;
            padding-right: 20px;
            /* Espacio entre la imagen y el texto */
        }

        @page {
            size: 102mm 130mm;
            margin: 0;
        }
    </style>
</head>

<body>
    @php
    $itemsPerPage = 5; // Cantidad de elementos por página
    $totalItems = count($labels); // Total de elementos
    $totalPages = ceil($totalItems / $itemsPerPage); // Total de páginas
    $pagesPerTotalAll = ceil($totalPages / $totalAll); // Páginas por totalAll
    @endphp

    @for ($totalAllPage = 1; $totalAllPage <= $totalAll; $totalAllPage++)
        @for ($page = 1; $page <= $pagesPerTotalAll; $page++)
            <table style="border: hidden; padding: 5%">
                <thead style="border: hidden">
                    <tr>
                        <th style="border: hidden">
                            <img src="{{ public_path('images/Alen.png') }}" alt="Alen" width="100%" height="auto">
                        </th>
                        <th>
                            <div class="header-text">
                                <p>folio: {{ $label->label }}</p>
                                <p>fecha: {{ $date->format('d/m/Y') }}</p>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody style="border: hidden">
                    <tr style="border: hidden">
                        <td colspan="2">
                            <table style="border-radius: 15px 15px 15px 15px;">
                                <thead>
                                    <tr>
                                        <th>Cliente</th>
                                        <th colspan="2">{{ $client->name }}</th>
                                    </tr>
                                    <tr>
                                        <th>Orden de venta</th>
                                        <th>Entrega</th>
                                        <th>Partidas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $startIndex = ($page - 1) * $itemsPerPage;
                                    $endIndex = min($startIndex + $itemsPerPage, $totalItems);
                                    @endphp
                                    @for ($i = $startIndex; $i < $endIndex; $i++)
                                        <tr>
                                            <td>
                                                @if (isset($labels[$i]))
                                                    {{ $labels[$i]->label }}
                                                @endif
                                            </td>
                                            <td>
                                                @if (isset($labels[$i]))
                                                    {{ $labels[$i]->delivery_order }}
                                                @endif
                                            </td>
                                            <td>
                                                @if (isset($labels[$i]))
                                                    {{ $labels[$i]->split }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: hidden">
                        <td colspan="2">
                            <table style="border-radius: 15px 15px 15px 15px;">
                                <thead>
                                    <tr>
                                        <td>{{ $totalCajas }}</td>
                                        <th>CAJAS</th>
                                        <td>{{ $totalPaquetes }}</td>
                                        <th>PAQUETES</th>
                                    </tr>
                                    <tr>
                                        <td>{{ $totalRollos }}</td>
                                        <th>ROLLOS</th>
                                        <td>{{ $totalCarrete }}</td>
                                        <th>CARRETE</th>
                                    </tr>
                                    <tr>
                                        <td>{{ $totalTarima }}</td>
                                        <th>TARIMA</th>
                                        <td>{{ $totalOtros }}</td>
                                        <th>{{ $Otros1 }}</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">{{ $TotalOtros1 }}</td>
                                        <th colspan="2">{{ $Otros2 }}</th>
                                    </tr>
                                </thead>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="border-radius: 15px 15px 15px 15px;">
                                <tbody>
                                    <tr>
                                        <td rowspan="2"></td>
                                        <td rowspan="2">{{ $totalAll }}</td>
                                        <td>Ancho</td>
                                    </tr>
                                    <tr>
                                        <td>Alto</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2">Total de empaques</td>
                                        <td rowspan="2">{{ $totalAllPage }} / {{ $totalAll }} </td>
                                        <td>Largo</td>
                                    </tr>
                                    <tr>
                                        <td>KGS</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        @endfor
    @endfor
</body>

</html>
