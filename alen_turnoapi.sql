-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 11, 2023 at 08:27 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alen_turnoapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rfc` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `compania` enum('Automatización','Electrica','Todas') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `rfc`, `compania`, `created_at`, `updated_at`) VALUES
(3, 'PUBLICO', '123j4lqrr', 'Electrica', '2023-06-22 03:22:07', '2023-06-27 20:28:38'),
(5, 'PUBLIC', '1234asdf1', 'Todas', '2023-06-22 04:21:28', '2023-06-22 04:21:28'),
(6, 'ALEN Veracruz', '1344sdaf', 'Todas', '2023-06-26 21:29:48', '2023-06-26 21:29:48'),
(7, 'ALEN Tijuana', '23442asdf', 'Todas', '2023-06-26 21:30:26', '2023-06-26 21:30:26'),
(17, 'prueba', '1234adf1', 'Electrica', '2023-06-27 01:59:15', '2023-06-27 01:59:15'),
(18, 'prueba2', '1234af11', 'Automatización', '2023-06-27 02:01:06', '2023-06-27 02:01:06'),
(19, 'alen', 'qeroqoq1', 'Electrica', '2023-06-27 02:02:00', '2023-06-27 02:02:00'),
(20, 'jhonatan', 'afljk134f1j', 'Automatización', '2023-06-28 04:31:37', '2023-06-28 04:31:37'),
(21, 'jhonat', '1234125', 'Electrica', '2023-06-29 00:40:13', '2023-06-29 00:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint UNSIGNED NOT NULL,
  `users_id` bigint UNSIGNED NOT NULL,
  `orders_id` bigint UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` enum('Ventas','Compras','EntregaSurtido','Empaque','Facturacion') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `users_id`, `orders_id`, `comment`, `department`, `created_at`, `updated_at`) VALUES
(1, 3, 13, 'prueba de comentario', 'Ventas', '2023-07-07 14:38:43', '2023-07-07 14:38:43'),
(2, 1, 5, 'prueba de comentario en una etiqueta', 'Ventas', '2023-07-10 22:30:15', '2023-07-10 22:30:15'),
(3, 1, 5, 'pendiente de pago', 'Ventas', '2023-07-11 03:00:28', '2023-07-11 03:00:28');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `id` bigint UNSIGNED NOT NULL,
  `types` enum('Normal','Traspaso') COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail_split` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `storehouseStart` enum('GDL','CDMX','TIJ','VER') COLLATE utf8mb4_unicode_ci NOT NULL,
  `storehouseEnd` enum('GDL','CDMX','TIJ','VER') COLLATE utf8mb4_unicode_ci NOT NULL,
  `orders_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `types`, `detail_split`, `storehouseStart`, `storehouseEnd`, `orders_id`, `created_at`, `updated_at`) VALUES
(1, 'Normal', '1,3', 'GDL', 'CDMX', 10, '2023-06-28 22:40:32', '2023-07-07 00:21:25'),
(2, 'Normal', '2', 'GDL', 'GDL', 10, '2023-06-28 22:48:30', '2023-07-07 00:21:47'),
(3, 'Normal', '1', 'GDL', 'GDL', 10, '2023-06-28 22:50:11', '2023-07-11 04:56:21'),
(4, 'Normal', '2,3', 'GDL', 'CDMX', 10, '2023-06-28 22:50:50', '2023-07-07 00:21:48'),
(5, 'Normal', '1', 'GDL', 'CDMX', 11, '2023-06-28 23:09:09', '2023-07-11 03:07:44'),
(6, 'Normal', '2,3', 'CDMX', 'CDMX', 11, '2023-06-28 23:16:13', '2023-07-07 00:21:54'),
(7, 'Normal', '4', 'GDL', 'CDMX', 11, '2023-06-28 23:18:38', '2023-07-11 04:56:25'),
(8, 'Normal', '1,3,5', 'GDL', 'GDL', 12, '2023-07-07 20:29:53', '2023-07-07 20:29:53'),
(9, 'Normal', '2,4', 'GDL', 'GDL', 12, '2023-07-07 20:30:22', '2023-07-11 04:56:29');

-- --------------------------------------------------------

--
-- Table structure for table `directions`
--

CREATE TABLE `directions` (
  `id` bigint UNSIGNED NOT NULL,
  `client` bigint UNSIGNED NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suburb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalCode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nameDirection` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referenceDirection` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `imagens`
--

CREATE TABLE `imagens` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labels_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imagens`
--

INSERT INTO `imagens` (`id`, `name`, `labels_id`, `created_at`, `updated_at`) VALUES
(39, 'Captura de pantalla 2023-03-06 082308.png', 6, '2023-07-08 02:00:10', '2023-07-08 02:00:10'),
(40, 'Captura de pantalla 2023-03-06 082308.png', 6, '2023-07-08 02:00:18', '2023-07-08 02:00:18'),
(41, 'Captura de pantalla 2023-03-06 082308.png', 6, '2023-07-08 02:00:27', '2023-07-08 02:00:27'),
(42, 'Captura de pantalla 2023-03-06 112116.png', 6, '2023-07-08 02:00:27', '2023-07-08 02:00:27'),
(43, 'Captura de pantalla 2023-03-06 112430.png', 6, '2023-07-08 02:00:27', '2023-07-08 02:00:27'),
(44, 'Captura de pantalla 2023-03-06 113343.png', 6, '2023-07-08 02:00:28', '2023-07-08 02:00:28'),
(45, 'Captura de pantalla 2023-03-06 114557.png', 6, '2023-07-08 02:00:28', '2023-07-08 02:00:28'),
(46, 'Captura de pantalla 2023-03-06 115501.png', 6, '2023-07-08 02:00:29', '2023-07-08 02:00:29'),
(47, 'Captura de pantalla 2023-03-06 115510.png', 6, '2023-07-08 02:00:30', '2023-07-08 02:00:30'),
(48, 'Captura de pantalla 2023-03-06 130851.png', 6, '2023-07-08 02:00:30', '2023-07-08 02:00:30'),
(53, 'Captura de pantalla 2023-03-06 114557.png', 10, '2023-07-12 01:36:42', '2023-07-12 01:36:42'),
(54, 'Captura de pantalla 2023-03-06 115501.png', 10, '2023-07-12 01:36:42', '2023-07-12 01:36:42'),
(55, 'Captura de pantalla 2023-03-06 115510.png', 10, '2023-07-12 01:36:43', '2023-07-12 01:36:43'),
(56, 'Captura de pantalla 2023-03-06 130851.png', 10, '2023-07-12 01:36:43', '2023-07-12 01:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` bigint UNSIGNED NOT NULL,
  `label` int NOT NULL,
  `delivery_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `split` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_folio` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Activo','Pendiente','Cerrado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `orders_id` bigint UNSIGNED NOT NULL,
  `users_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `label`, `delivery_order`, `split`, `invoice_folio`, `file_invoice`, `status`, `orders_id`, `users_id`, `created_at`, `updated_at`) VALUES
(5, 1505, '1235', '1,2,3', NULL, NULL, 'Pendiente', 5, 1, '2023-06-30 04:17:41', '2023-06-30 04:17:41'),
(6, 1506, '125', '4,5', NULL, NULL, 'Pendiente', 5, 1, '2023-06-30 04:20:24', '2023-06-30 23:33:18'),
(9, 1507, '123566', '1,2,4', NULL, NULL, 'Pendiente', 6, 1, '2023-06-30 20:08:56', '2023-06-30 23:33:02'),
(10, 1508, '3456', '3,5', NULL, NULL, 'Pendiente', 6, 1, '2023-06-30 20:09:48', '2023-06-30 23:51:16'),
(11, 1509, '12355', '1,2,3', NULL, NULL, 'Pendiente', 10, 1, '2023-07-06 01:55:42', '2023-07-06 01:57:42'),
(12, 4321, '5045', '1,2,3', NULL, NULL, 'Pendiente', 13, 3, '2023-07-07 14:51:38', '2023-07-10 22:36:12'),
(13, 4322, '1234', '3', NULL, NULL, 'Activo', 8, 1, '2023-07-11 22:09:38', '2023-07-11 22:09:38'),
(14, 4323, '234', '4', NULL, NULL, 'Activo', 8, 1, '2023-07-11 22:10:24', '2023-07-11 22:10:24'),
(15, 4324, '12356', '4', NULL, NULL, 'Pendiente', 7, 1, '2023-07-11 22:16:14', '2023-07-12 02:23:09'),
(16, 4325, '1233', '5', NULL, NULL, 'Pendiente', 7, 1, '2023-07-11 22:16:53', '2023-07-12 02:23:06'),
(17, 4326, '235', '3', NULL, NULL, 'Pendiente', 8, 1, '2023-07-11 22:17:33', '2023-07-12 02:23:03'),
(18, 4327, '34232', '2', NULL, NULL, 'Pendiente', 7, 1, '2023-07-11 22:20:52', '2023-07-12 02:22:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_06_19_141228_create_clients_table', 1),
(6, '2023_06_19_141810_create_sap_keys_table', 1),
(7, '2023_06_19_141950_create_directions_table', 1),
(8, '2023_06_19_142110_create_orders_table', 1),
(9, '2023_06_19_142132_create_labels_table', 1),
(10, '2023_06_19_142139_create_times_table', 1),
(11, '2023_06_19_142153_create_comments_table', 1),
(12, '2023_06_19_142350_create_packing__details_table', 1),
(13, '2023_06_19_142417_create_imagens_table', 1),
(14, '2023_06_19_143012_create_details_table', 1),
(15, '2023_06_19_152717_permissions', 1),
(16, '2023_06_19_152904_roles', 1),
(17, '2023_06_19_155053_create_pre_configurations_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint UNSIGNED NOT NULL,
  `order` int UNSIGNED NOT NULL,
  `typeDelivery` enum('Normal','Inmediata','El cliente pasa') COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `users_id` bigint UNSIGNED NOT NULL,
  `sap_keys_id` bigint UNSIGNED NOT NULL,
  `process` enum('Ventas','Compras','EntregaSurtido','Empaque','Facturacion','Logistica','Ruta','Envio','Terminado') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Activa','Pendiente','Cerrada') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order`, `typeDelivery`, `date`, `users_id`, `sap_keys_id`, `process`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 123456, 'Normal', '2023-06-26 16:40:29', 1, 5, 'Ventas', 'Activa', '2023-06-26 22:48:26', '2023-06-26 22:42:29', '2023-06-26 22:48:26'),
(2, 12345, 'Normal', '2023-06-27 23:25:15', 1, 11, 'EntregaSurtido', 'Activa', '2023-06-29 03:57:32', '2023-06-28 03:25:37', '2023-06-29 03:57:32'),
(3, 23445, 'Normal', '2023-06-30 17:20:47', 1, 13, 'Ventas', 'Activa', '2023-06-29 04:40:32', '2023-06-28 22:13:42', '2023-06-29 04:40:32'),
(4, 2355, 'Normal', '2023-06-30 17:35:56', 1, 11, 'Ventas', 'Activa', '2023-06-29 04:41:34', '2023-06-28 22:15:24', '2023-06-29 04:41:34'),
(5, 2365, 'Inmediata', '2023-06-30 17:35:56', 1, 11, 'Empaque', 'Activa', NULL, '2023-06-28 22:17:42', '2023-07-10 20:42:12'),
(6, 4335, 'El cliente pasa', '2023-07-10 18:19:03', 1, 14, 'Empaque', 'Activa', NULL, '2023-06-28 22:19:55', '2023-07-10 20:42:15'),
(7, 4356, 'Normal', '2023-07-19 17:30:20', 1, 14, 'Empaque', 'Activa', NULL, '2023-06-28 22:23:19', '2023-07-12 02:22:58'),
(8, 11111, 'Inmediata', '2023-06-28 16:26:17', 1, 16, 'Empaque', 'Activa', NULL, '2023-06-28 22:26:38', '2023-07-12 02:23:03'),
(9, 53535, 'El cliente pasa', '2023-06-28 16:29:44', 1, 5, 'EntregaSurtido', 'Activa', NULL, '2023-06-28 22:30:39', '2023-06-30 20:10:12'),
(10, 567890, 'Normal', '2023-06-28 16:38:00', 1, 13, 'EntregaSurtido', 'Activa', NULL, '2023-06-28 22:40:15', '2023-07-07 03:42:20'),
(11, 6787, 'Normal', '2023-06-28 17:07:51', 1, 16, 'EntregaSurtido', 'Activa', NULL, '2023-06-28 23:09:00', '2023-07-07 03:42:22'),
(12, 123444, 'Normal', '2023-07-11 08:23:21', 3, 5, 'EntregaSurtido', 'Activa', NULL, '2023-07-07 14:23:21', '2023-07-11 20:31:16'),
(13, 123556, 'Normal', '2023-07-12 08:23:21', 3, 5, 'Empaque', 'Activa', NULL, '2023-07-07 20:25:30', '2023-07-10 22:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `packing_details`
--

CREATE TABLE `packing_details` (
  `id` bigint UNSIGNED NOT NULL,
  `box` int UNSIGNED NOT NULL,
  `pallet` int UNSIGNED NOT NULL,
  `roll` int UNSIGNED NOT NULL,
  `rell` int UNSIGNED NOT NULL,
  `pack` int UNSIGNED NOT NULL,
  `optionDatalist` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option` int UNSIGNED NOT NULL,
  `optionDatalist1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option1` int UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `labels_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packing_details`
--

INSERT INTO `packing_details` (`id`, `box`, `pallet`, `roll`, `rell`, `pack`, `optionDatalist`, `option`, `optionDatalist1`, `option1`, `comment`, `labels_id`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 0, 0, 0, '0', 0, '0', 0, NULL, 6, '2023-06-30 19:59:46', '2023-06-30 19:59:46'),
(2, 1, 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, 5, '2023-07-12 00:02:42', '2023-07-12 00:02:42'),
(6, 0, 0, 0, 0, 0, NULL, 0, NULL, 0, NULL, 9, '2023-07-12 00:06:16', '2023-07-12 00:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'order.views', 'web', '2023-06-22 00:28:56', '2023-06-22 00:28:56'),
(2, 'order.create', 'web', '2023-06-22 00:28:56', '2023-06-22 00:28:56'),
(3, 'order.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(4, 'order.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(5, 'order.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(6, 'transfers.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(7, 'transfers.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(8, 'transfers.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(9, 'transfers.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(10, 'transfers.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(11, 'deliverys.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(12, 'deliverys.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(13, 'deliverys.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(14, 'deliverys.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(15, 'deliverys.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(16, 'paking.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(17, 'paking.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(18, 'paking.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(19, 'paking.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(20, 'paking.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(21, 'invoicing.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(22, 'invoicing.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(23, 'invoicing.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(24, 'invoicing.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(25, 'invoicing.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(26, 'quality.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(27, 'quality.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(28, 'quality.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(29, 'quality.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(30, 'quality.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(31, 'report.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(32, 'report.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(33, 'report.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(34, 'report.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(35, 'report.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(36, 'client.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(37, 'client.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(38, 'client.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(39, 'client.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(40, 'client.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(41, 'user.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(42, 'user.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(43, 'user.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(44, 'user.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(45, 'user.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(46, 'direction.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(47, 'direction.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(48, 'direction.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(49, 'direction.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(50, 'direction.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(51, 'label.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(52, 'label.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(53, 'label.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(54, 'label.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(55, 'label.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(56, 'splits.views', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(57, 'splits.create', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(58, 'splits.read', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(59, 'splits.update', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(60, 'splits.delete', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'token', '3daedf132dd772fbe01fd7d82da54cc15a8ae510ea25800b62645c8ceb304d46', '[\"*\"]', '2023-06-26 23:13:46', NULL, '2023-06-22 00:31:26', '2023-06-26 23:13:46'),
(2, 'App\\Models\\User', 1, 'token', 'd9ac53abd217c1cbf32e876a59c4536370126ae27a8acb8a487454bd10ec0207', '[\"*\"]', '2023-07-07 04:36:51', NULL, '2023-06-22 02:12:46', '2023-07-07 04:36:51'),
(3, 'App\\Models\\User', 1, 'token', '407481d8f2c2a3de4ccfdbcb6fe7d7c8130455174e93e59b64ba09a6a74cfb30', '[\"*\"]', '2023-07-04 22:50:53', NULL, '2023-06-26 20:54:51', '2023-07-04 22:50:53'),
(4, 'App\\Models\\User', 1, 'token', '9fa5d12b4fa2043cac1b261921b51bc079689eb94b53f89b230c15d67f3101a9', '[\"*\"]', '2023-07-07 22:36:45', NULL, '2023-06-26 23:14:34', '2023-07-07 22:36:45'),
(5, 'App\\Models\\User', 3, 'token', '8f8e7d8407e5c335231da8121cee0893f9337940c62118c356ad9baf6edf0477', '[\"*\"]', '2023-07-12 01:31:44', NULL, '2023-07-07 20:21:07', '2023-07-12 01:31:44'),
(6, 'App\\Models\\User', 3, 'token', '7d37765bc9994eafd45989e9a7b7bd8c881245fbce975cbe67d8fcce9fa5b6a6', '[\"*\"]', '2023-07-07 22:51:46', NULL, '2023-07-07 22:48:52', '2023-07-07 22:51:46'),
(7, 'App\\Models\\User', 1, 'token', '5acfe15ddcdc8963258ca5886cd364630fe2dada597b665be92acd3123890aaa', '[\"*\"]', '2023-07-08 02:00:26', NULL, '2023-07-07 22:58:08', '2023-07-08 02:00:26'),
(8, 'App\\Models\\User', 1, 'token', '17ff58315cdd086389152fade3ffd03999011f5e2bb04eafcf1b0fbc21a85a74', '[\"*\"]', '2023-07-10 20:44:30', NULL, '2023-07-07 22:58:19', '2023-07-10 20:44:30'),
(9, 'App\\Models\\User', 1, 'token', '561f53479a0009ab254917d1dcf013cc1d90481cdc439d2ec4067b963db55d74', '[\"*\"]', '2023-07-12 02:23:10', NULL, '2023-07-10 20:48:26', '2023-07-12 02:23:10');

-- --------------------------------------------------------

--
-- Table structure for table `pre_configurations`
--

CREATE TABLE `pre_configurations` (
  `id` bigint UNSIGNED NOT NULL,
  `typeCharters` enum('A Domicilio','Ocurre') COLLATE utf8mb4_unicode_ci NOT NULL,
  `methods` enum('Cob. Reg','Credito','Pagado','X cobrar') COLLATE utf8mb4_unicode_ci NOT NULL,
  `idDirection` bigint UNSIGNED NOT NULL,
  `typeDocument` enum('Directo','Reembarque','Ruta') COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` enum('GDL','CDMX','TIJ','VER','TX','PAN') COLLATE utf8mb4_unicode_ci NOT NULL,
  `parcelOffices` enum('GDL','CDMX','TIJ','VER','TX','PAN') COLLATE utf8mb4_unicode_ci NOT NULL,
  `idDriver` bigint UNSIGNED NOT NULL,
  `clients_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2023-06-22 00:28:57', '2023-06-22 00:28:57'),
(2, 'Ventas', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58'),
(3, 'Compras', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58'),
(4, 'Surtidos', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58'),
(5, 'Empaque', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58'),
(6, 'Facturacion', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58'),
(7, 'Logistica', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58'),
(8, 'Calidad', 'web', '2023-06-22 00:28:58', '2023-06-22 00:28:58');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint UNSIGNED NOT NULL,
  `role_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 4),
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 5),
(21, 6),
(22, 6),
(23, 6),
(24, 6),
(25, 6),
(26, 8),
(27, 8),
(28, 8),
(29, 8),
(30, 8);

-- --------------------------------------------------------

--
-- Table structure for table `sap_keys`
--

CREATE TABLE `sap_keys` (
  `id` bigint UNSIGNED NOT NULL,
  `key_sap` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clients_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sap_keys`
--

INSERT INTO `sap_keys` (`id`, `key_sap`, `clients_id`, `created_at`, `updated_at`) VALUES
(5, 'C-pub', 3, '2023-06-26 21:10:40', '2023-06-26 21:10:40'),
(6, 'c-pubsas', 3, '2023-06-26 18:06:27', '2023-06-26 18:06:27'),
(8, 'c-pru', 17, '2023-06-27 01:59:15', '2023-06-27 01:59:15'),
(9, 'c-pru2', 18, '2023-06-27 02:01:06', '2023-06-27 02:01:06'),
(10, 'C-ALEN', 19, '2023-06-27 02:02:00', '2023-06-27 02:02:00'),
(11, 'c-jho', 3, '2023-06-27 20:22:24', '2023-06-27 20:22:24'),
(12, 'c-plu', 3, '2023-06-27 20:28:56', '2023-06-27 20:28:56'),
(13, 'c-pud', 5, '2023-06-28 04:17:37', '2023-06-28 04:17:37'),
(14, 'c-alV', 6, '2023-06-28 04:17:51', '2023-06-28 04:17:51'),
(15, 'c-alT', 7, '2023-06-28 04:18:52', '2023-06-28 04:18:52'),
(16, 'c-jont', 20, '2023-06-28 04:31:37', '2023-06-28 04:31:37'),
(17, 'c-jnot', 21, '2023-06-29 00:40:13', '2023-06-29 00:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE `times` (
  `id` bigint UNSIGNED NOT NULL,
  `department` enum('Ventas','Compras','EntregaSurtido','Empaque','Facturacion') COLLATE utf8mb4_unicode_ci NOT NULL,
  `orders_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` enum('GDL','CDMX','TIJ','VER','TX','PAN') COLLATE utf8mb4_unicode_ci NOT NULL,
  `departament` enum('All','Ventas','Compras','Surtido','Empaque') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user`, `email`, `email_verified_at`, `password`, `location`, `departament`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Jhonatan Miguel Angel Navarro Mejia', 'jnavarro', 'jnavarro@alenintelligent.com', NULL, '$2y$10$h4c8QydcLER7i1m9mYvFs.NOrwmetkPILDIci4pSm3WgWin1ytLJC', 'GDL', 'All', NULL, NULL, '2023-06-22 00:30:34', '2023-06-22 00:30:34'),
(2, 'prueba', 'ppp', 'pp@alenintelligent.com', '2023-07-07 14:15:26', '12345', 'GDL', 'Ventas', NULL, NULL, '2023-07-07 14:15:26', '2023-07-07 14:15:26'),
(3, 'Nancy Flores Hidalgo', 'nflores', 'nflores@alenintelligent.com', NULL, '$2y$10$o0x/LqbRKMljnshFduyzGOKzdfMwkFoAfdJcyYitwGsH9iNCXY5nK', 'GDL', 'Ventas', NULL, NULL, '2023-07-07 20:20:48', '2023-07-07 20:20:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_Clients` (`name`,`rfc`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_users_id_foreign` (`users_id`),
  ADD KEY `comments_orders_id_foreign` (`orders_id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `details_orders_id_foreign` (`orders_id`);

--
-- Indexes for table `directions`
--
ALTER TABLE `directions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `directions_client_foreign` (`client`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `imagens`
--
ALTER TABLE `imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `imagens_labels_id_foreign` (`labels_id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `labels_orders_id_foreign` (`orders_id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_users_id_foreign` (`users_id`),
  ADD KEY `orders_sap_keys_id_foreign` (`sap_keys_id`);

--
-- Indexes for table `packing_details`
--
ALTER TABLE `packing_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packing_details_labels_id_foreign` (`labels_id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `pre_configurations`
--
ALTER TABLE `pre_configurations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pre_configurations_iddirection_foreign` (`idDirection`),
  ADD KEY `pre_configurations_iddriver_foreign` (`idDriver`),
  ADD KEY `pre_configurations_clients_id_foreign` (`clients_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sap_keys`
--
ALTER TABLE `sap_keys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sap_keys_clients_id_foreign` (`clients_id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`id`),
  ADD KEY `times_orders_id_foreign` (`orders_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `directions`
--
ALTER TABLE `directions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imagens`
--
ALTER TABLE `imagens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `packing_details`
--
ALTER TABLE `packing_details`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pre_configurations`
--
ALTER TABLE `pre_configurations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sap_keys`
--
ALTER TABLE `sap_keys`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `comments_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `details`
--
ALTER TABLE `details`
  ADD CONSTRAINT `details_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `directions`
--
ALTER TABLE `directions`
  ADD CONSTRAINT `directions_client_foreign` FOREIGN KEY (`client`) REFERENCES `clients` (`id`);

--
-- Constraints for table `imagens`
--
ALTER TABLE `imagens`
  ADD CONSTRAINT `imagens_labels_id_foreign` FOREIGN KEY (`labels_id`) REFERENCES `labels` (`id`);

--
-- Constraints for table `labels`
--
ALTER TABLE `labels`
  ADD CONSTRAINT `labels_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `labels_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_sap_keys_id_foreign` FOREIGN KEY (`sap_keys_id`) REFERENCES `sap_keys` (`id`),
  ADD CONSTRAINT `orders_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `packing_details`
--
ALTER TABLE `packing_details`
  ADD CONSTRAINT `packing_details_labels_id_foreign` FOREIGN KEY (`labels_id`) REFERENCES `labels` (`id`);

--
-- Constraints for table `pre_configurations`
--
ALTER TABLE `pre_configurations`
  ADD CONSTRAINT `pre_configurations_clients_id_foreign` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `pre_configurations_iddirection_foreign` FOREIGN KEY (`idDirection`) REFERENCES `directions` (`id`),
  ADD CONSTRAINT `pre_configurations_iddriver_foreign` FOREIGN KEY (`idDriver`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sap_keys`
--
ALTER TABLE `sap_keys`
  ADD CONSTRAINT `sap_keys_clients_id_foreign` FOREIGN KEY (`clients_id`) REFERENCES `clients` (`id`);

--
-- Constraints for table `times`
--
ALTER TABLE `times`
  ADD CONSTRAINT `times_orders_id_foreign` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
