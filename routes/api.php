<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
// Usuario check
Route::post('register', [AuthController::class, "register"]);
Route::post('login', [AuthController::class, 'login']);
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('user-profile', [AuthController::class, "userProfile"]);
    Route::get('user', 'App\Http\Controllers\AuthController@index');
    Route::put('user/{user}', 'App\Http\Controllers\AuthController@update');
    Route::delete('user/{user}', 'App\http\Controllers\AuthController@destroy');
    // route for key_saps
    Route::prefix('sapkeys')->group(function () {
        Route::get('/', 'App\Http\Controllers\SapKeysController@index');
        Route::post('/', 'App\Http\Controllers\SapKeysController@store');
        Route::get('{sap_keys}', 'App\Http\Controllers\SapKeysController@show');
        Route::put('{sap_keys}', 'App\Http\Controllers\SapKeysController@update');
        Route::delete('{sap_keys}', 'App\Http\Controllers\SapKeysController@destroy');
    });
    // route for clients
    Route::prefix("clients")->group(function () {
        Route::get('/', 'App\Http\Controllers\ClientsController@index');
        Route::post('/', 'App\Http\Controllers\ClientsController@store');
        Route::get('{clients}', 'App\Http\Controllers\ClientsController@show');
        Route::put('{clients}', 'App\Http\Controllers\ClientsController@update');
        Route::delete('{clients}', 'App\Http\Controllers\ClientsController@destroy');
    });
    // route form Order
    Route::prefix("orders")->group(function () {
        Route::get('/', 'App\Http\Controllers\OrdersController@index');
        Route::post('/', 'App\Http\Controllers\OrdersController@store');
        Route::get('{orders}', 'App\Http\Controllers\OrdersController@show');
        Route::get('s/{orders}', 'App\Http\Controllers\OrdersController@showOrder');
        Route::put('{orders}', 'App\Http\Controllers\OrdersController@update');
        Route::put('orderProcess/{orders}', 'App\Http\Controllers\OrdersController@Process');
        Route::delete('{orders}', 'App\Http\Controllers\OrdersController@destroy');
    });

    Route::prefix('tranfers')->group(function () {
        Route::get('/', 'App\Http\Controllers\OrdersController@showTranfers');
    });

    Route::prefix('delivery')->group(function () {
        Route::get('/', 'App\Http\Controllers\OrdersController@showDelivery');
    });

    Route::prefix('invoice')->group(function () {
        Route::get('/', 'App\Http\Controllers\OrdersController@showOrderInvoice');
    });
    // route form Label
    Route::prefix("labels")->group(function () {
        Route::get('/', 'App\Http\Controllers\LabelsController@index');
        Route::post('/', 'App\Http\Controllers\labelsController@store');
        Route::post('file/{labels}', 'App\Http\Controllers\labelsController@storeFile');
        Route::get('{orders_id}', 'App\Http\Controllers\labelsController@show');
        Route::get('showL/{orders_id}', 'App\Http\Controllers\labelsController@showLabel');
        Route::put('{labels}', 'App\Http\Controllers\labelsController@process');
        Route::delete('{labels}', 'App\Http\Controllers\LabelsController@destroy');
    });
    Route::put('/consolidate/{labels}', 'App\Http\Controllers\LabelsController@updateLink');
    Route::get('/generate-label-pdf/{labels}', 'App\Http\Controllers\LabelsController@generatePDF');
    Route::prefix("labelsI")->group(function () {
        Route::get('{labels}', 'App\Http\Controllers\LabelsController@showLabelImg');
    });

    Route::prefix("labelLast")->group(function () {
        Route::get('/', 'App\Http\Controllers\LabelsController@showLast');
    });

    Route::prefix("allLabel")->group(function () {
        Route::get('/', "App\Http\Controllers\LabelsController@indexLabel");
    });

    Route::prefix("details")->group(function () {
        Route::get('/', 'App\Http\Controllers\DetailsController@index');
        Route::post('/', 'App\Http\Controllers\DetailsController@store');
        Route::get('{orders_id}', 'App\Http\Controllers\DetailsController@show');
        Route::put('{details}', 'App\Http\Controllers\DetailsController@update');
        Route::delete('{id}', 'App\Http\Controllers\DetailsController@destroy');
    });
    // route for comments create,read,update,delete check 
    Route::prefix("comment")->group(function () {
        Route::get('/', 'App\Http\Controllers\CommentsController@index');
        Route::post('/', 'App\Http\Controllers\CommentsController@store');
        Route::get('{comments}', 'App\Http\Controllers\CommentsController@show');
        Route::put('{Comments}', 'App\Http\Controllers\CommentsController@update');
        Route::delete('{Comments}', 'App\Http\Controllers\CommentsController@destroy');
    });
    // route for imagen create, read,update, deleteE
    Route::prefix("packing")->group(function () {
        Route::get('/', 'App\Http\Controllers\PackingDetailsController@index');
        Route::post('/', 'App\Http\Controllers\PackingDetailsController@store');
        Route::get('{label_id}', 'App\Http\Controllers\PackingDetailsController@show');
        Route::put('{packingDetails}', 'App\Http\Controllers\PackingDetailsController@update');
    });

    Route::prefix("imagen")->group(function () {
        Route::get('/', 'App\Http\Controllers\ImagensController@index');
        Route::post('/', 'App\Http\Controllers\ImagensController@store');
        Route::get('{imagen}', 'App\Http\Controllers\ImagensController@show');
        Route::delete('{imagen}', 'App\Http\Controllers\ImagensController@destroy');
    });

    Route::prefix("unios")->group(function () {
        Route::get("/", "App\Http\Controllers\UnionsController@index");
        Route::post("/", "App\Http\Controllers\UnionsController@store");
        Route::get("{id}", "App\Http\Controllers\UnionsController@show");
        Route::get('labels/{labels}', 'App\Http\Controllers\UnionsController@find');
        Route::get("{unions}", "App\Http\Controllers\UnionsController@destroy");
    });

    Route::prefix('consolidation')->group(function () {
        Route::get('{labels}', 'App\Http\Controllers\ConsolidatedController@index');
        Route::post('/', 'App\Http\Controllers\ConsolidatedController@store');
    });
    Route::get('search', 'App\Http\Controllers\Search@search');
});

Route::get('imagenP/{filepath}', 'App\Http\Controllers\ImagensController@showP');