<?php

namespace App\Events;

use App\Models\Comments;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $comment;

    /**
     * Create a new event instance.
     *
     * @param  Comments  $comment
     * @return void
     */
    public function __construct(Comments $comment)
    {
        $this->comment = $comment;
    }


    public function broadcastOn()
    {
        // return ['alenTurno'];
        return [new PrivateChannel('App.User.' . $this->comment->users_id)];
    }

    public function broadcastAs()
    {
        return 'alenTurno';
    }
}