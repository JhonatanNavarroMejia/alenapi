<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unions extends Model
{
    use HasFactory;

    public function Labels()
    {
        return $this->belongsTo(Labels::class, 'label_id', 'id');
    }
}