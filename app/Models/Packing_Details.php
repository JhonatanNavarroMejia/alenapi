<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Packing_Details extends Model
{
    use HasFactory;

    protected $table = "packing_details";

    public function Labels()
    {
        return $this->belongsTo(Labels::class);
    }
}