<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Labels extends Model
{
    use HasFactory, Searchable;

    public function toSearchableArray()
    {
        return [
            'label' => $this->label,
            'delivery_order' => $this->delivery_order,
            'invoice_folio' => $this->invoice_folio,
        ];
    }

    public function Orders()
    {
        return $this->belongsTo(Orders::class);
    }

    public function Imagens()
    {
        return $this->hasMany(Imagens::class);
    }

    public function packing_details()
    {
        return $this->hasOne(Packing_Details::class);
    }

    public function Users()
    {
        return $this->belongsTo(User::class);
    }

    public function Unions()
    {
        return $this->belongsTo(Unions::class, 'id', 'labels_id');
    }

    public function Consolidated()
    {
        return $this->belongsTo(consolidated::class);
    }
}