<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class consolidated extends Model
{
    use HasFactory;

    public function Label()
    {
        return $this->hasMany(Labels::class);
    }
}