<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    // protected $dates = ['delete_at'];

    public function Directions()
    {
        return $this->hasMany(Direction::class);
    }

    public function SapKeys()
    {
        return $this->hasMany(sap_keys::class);
    }

}