<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Orders extends Model
{
    use HasFactory, SoftDeletes, Searchable;

    protected $dates = ['delete_at'];

    public function toSearchableArray()
    {
        return [
            'order' => $this->order,
            'date' => $this->date,
            'process' => $this->process
        ];
    }

    public function Users()
    {
        return $this->belongsTo(User::class);
    }

    public function SapKeys()
    {
        return $this->belongsTo(sap_keys::class);
    }

    public function Comments()
    {
        return $this->belongsTo(Comments::class);
    }

    public function Transfers()
    {
        return $this->hasMany(Transfers::class);
    }

    public function Labels()
    {
        return $this->hasMany(Labels::class);
    }

    public function Details()
    {
        return $this->hasMany(Details::class);
    }

    public function Times()
    {
        return $this->belongsTo(Time::class);
    }
}