<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sap_keys extends Model
{
    use HasFactory;

    public function Orders()
    {
        return $this->hasMany(Orders::class);
    }

    public function Clients()
    {
        return $this->belongsTo(Clients::class);
    }
}