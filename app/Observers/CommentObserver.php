<?php

namespace App\Observers;

use App\Models\Comments;

class CommentObserver
{
    /**
     * Handle the Comment "created" event.
     */
    public function created(Comments $comment): void
    {
        //
    }

    /**
     * Handle the Comment "updated" event.
     */
    public function updated(Comments $comment): void
    {
        //
    }

    /**
     * Handle the Comment "deleted" event.
     */
    public function deleted(Comments $comment): void
    {
        //
    }

    /**
     * Handle the Comment "restored" event.
     */
    public function restored(Comments $comment): void
    {
        //
    }

    /**
     * Handle the Comment "force deleted" event.
     */
    public function forceDeleted(Comments $comment): void
    {
        //
    }
}