<?php

namespace App\Observers;

use App\Mail\TransferEmail;
use App\Models\Details;
use Illuminate\Support\Facades\Mail;

class DetailObserver
{
    public function created(Details $details)
    {
        if ($details->types === 'Traspaso') {
            info('Sending email for Transfer type...');
            Mail::to('compras@alenintelligent.com')->send(new TransferEmail($details));
        }
    }
}