<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ParckingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'box' => $this->box,
            'pallet' => $this->pallet,
            'roll' => $this->roll,
            'rell' => $this->rell,
            'pack' => $this->pack,
            'optionDatalist' => $this->optionDatalist,
            'option' => $this->option,
            'optionDatalist1' => $this->optionDatalist1,
            'option1' => $this->option1,
            'comment' => $this->comment,
            'label' => $this->labels->label ?? null,
            'delivery' => $this->labels->delivery_order ?? null,
            'split' => $this->labels->split ?? null,
            'order' => $this->labels->orders->order ?? null,
            'name' => $this->labels->orders->sap_keys->clients->name ?? null,
        ];

    }
}