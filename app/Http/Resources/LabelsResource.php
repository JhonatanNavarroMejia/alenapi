<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LabelsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'delivery' => $this->delivery_order,
            'split' => $this->split,
            "invoice_folio" => $this->invoice,
            "file_invoice" => $this->file,
            "status" => $this->status,
            "user" => $this->users->location,
            "packing_details" => $this->packing_details,
            "client" => $this->orders->sapkeys->clients->name,
            "order_id" => $this->orders->id,
            "order" => $this->orders->order,

            "type" => $this->orders->typeDelivery,
            "consolidate" => optional($this->consolidated)->Store,
        ];
        // return parent::toArray($request);
    }
}