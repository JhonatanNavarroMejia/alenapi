<?php

namespace App\Http\Controllers;

use App\Models\Details;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DetailsController extends Controller
{
    public function index()
    {
        $orders = Orders::with('SapKeys.Clients', 'Details')
            ->whereHas('Details', function ($query) {
                $query->where('types', 'Traspaso');
            })
            ->get();
        return Response()->json($orders, Response::HTTP_OK);
    }

    public function delivery()
    {
        $order = Orders::with('SapKeys.Clients', 'Details')
            ->where(function ($query) {
                $query->where('process', 'Compras')
                    ->orWhere('process', 'EntregaSurtido');
            })
            ->whereDoesntHave('Details', function ($Details) {
                $Details->where('types', 'Details');
            });

        return Response()->json($order->get(), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:Traspaso,Normal',
            'details_splits' => 'required',
            'companyO' => 'required',
            'originStore' => 'required|in:01,02,03,04',
            'companyD' => "required",
            'destinationStore' => 'required|in:01,02,03,04',
            'orders_id' => 'required'
        ]);

        $details = new Details;
        $details->types = $request->type;
        $details->detail_split = $request->details_splits;
        $details->companyO = $request->companyO;
        $details->storehouseStart = $request->originStore;
        $details->companyD = $request->companyD;
        $details->storehouseEnd = $request->destinationStore;
        $details->orders_id = $request->orders_id;
        $details->save();

        return response()->json($details, Response::HTTP_CREATED);
    }
    public function show($labels_id)
    {
        $details = Details::where("orders_id", $labels_id)->get();
        return response()->json($details, Response::HTTP_OK);
    }

    public function update(Request $request, Details $details)
    {
        $request->validate([
            "types" => 'required|in:Traspaso,Normal',
        ]);
        $details->types = $request->types;
        $details->save();
        return response()->json(['UpdateProcess' => $details]);
    }

    public function destroy($id)
    {
        $details = Details::where("id", $id)->delete();
        $data = [
            'message' => 'Details splits delete',
            'Details' => $details
        ];

        return response()->json($data, Response::HTTP_OK);
    }
}