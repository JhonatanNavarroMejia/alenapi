<?php

namespace App\Http\Controllers;

use App\Http\Resources\ParckingResource;
use App\Models\Packing_Details;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PackingDetailsController extends Controller
{
    public function index()
    {
        $packing = Packing_Details::all();
        return response()->json($packing, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            "box" => "required|numeric",
            "pallet" => "required|numeric",
            "roll" => "required|numeric",
            "rell" => "required|numeric",
            "packing" => "required|numeric",
            "optionI" => "required|numeric",
            "optionI1" => "required|numeric",
            "label" => "required|numeric"
        ], [
            "box.required" => "Falta la caja",
            "pallet.required" => "Falta la tarima",
            "roll.required" => "Falta el rollo",
            "rell.required" => "Falta el carrete",
            "packing.required" => "Falta el paquete",
            "option.required" => "Falta las optiones",
            "optionI1.required" => "Falta las optiones",
            "label.required" => "Recarga la pagina"
        ]);
        $split = new Packing_Details;
        $split->box = $request->box;
        $split->pallet = $request->pallet;
        $split->roll = $request->roll;
        $split->rell = $request->rell;
        $split->pack = $request->packing;
        $split->optionDatalist = $request->optionD;
        $split->option = $request->optionI;
        $split->optionDatalist1 = $request->optionD1;
        $split->option1 = $request->optionI1;
        $split->comment = $request->comment;
        $split->labels_id = $request->label;
        $split->save();

        return response()->json($split, Response::HTTP_CREATED);
    }

    public function show($labels_id)
    {
        $packing = Packing_Details::with('labels.Orders.SapKeys.Clients')->where("labels_id", $labels_id)->first();
        $packingR = new ParckingResource($packing);
        return response()->json($packingR, Response::HTTP_FOUND);
    }

    public function update(Request $request, Packing_Details $packing)
    {
        $request->validate([
            "label" => "required|numeric",
            "box" => "required|numeric",
            "pallet" => "required|numeric",
            "roll" => "required|numeric",
            "rell" => "required|numeric",
            "packing" => "required|numeric",
            "optionD" => "required|numeric",
            "optionI" => "required|numeric",
            "optionD1" => "required|numeric",
            "optionI1" => "required|numeric",
        ]);

        $packing->box = $request->box;
        $packing->pallet = $request->pallet;
        $packing->roll = $request->roll;
        $packing->rell = $request->rell;
        $packing->pack = $request->packing;
        $packing->optionD = $request->optionD;
        $packing->optionI = $request->optionI;
        $packing->optionD1 = $request->optionD1;
        $packing->option1 = $request->optionI1;
        $packing->labels_id = $request->label;
        $packing->save();

        return response()->json($packing, Response::HTTP_OK);
    }
}