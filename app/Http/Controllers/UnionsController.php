<?php

namespace App\Http\Controllers;

use App\Models\Labels;
use App\Models\Unions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UnionsController extends Controller
{
    public function index()
    {
        $unions = Unions::pluck('labels_id')->toArray();
        $orders = Labels::with("Imagens", "packing_details")->whereNotIn('id', $unions)->get();
        return response()->json($orders);
    }

    public function store(Request $request)
    {
        $request->validate([
            "label_id" => "required|numeric",
            "labels_id" => "required|numeric"
        ]);

        $unions = new Unions;
        $unions->label_id = $request->label_id;
        $unions->labels_id = $request->labels_id;
        $unions->save();

        return response()->json($unions, Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $uniosLabel = Unions::with("labels")->where("label_id", $id)->get();
        return response()->json($uniosLabel, Response::HTTP_OK);
    }

    public function find(Labels $labels)
    {
        $clientId = $labels->Orders->SapKeys->Clients->id;

        // Obtener todas las etiquetas asociadas al cliente
        $labelsForClient = Labels::whereHas('orders.sapKeys.clients', function ($query) use ($clientId) {
            $query->where('clients.id', $clientId);
        })->get();

        $labelsNotInUnions = Labels::whereDoesntHave('Unions')->get();

        $allLabels = $labelsForClient->intersect($labelsNotInUnions);

        return response()->json($allLabels, Response::HTTP_ACCEPTED);
    }

    public function destroy(Unions $unions)
    {
        $unions->delete();
        return response()->json($unions, Response::HTTP_NO_CONTENT);
    }
}