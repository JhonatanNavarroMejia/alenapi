<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\Time;
use App\Models\Unions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $perPage = $request->per_pages;
        $user = auth()->user()->id;
        $orders = Orders::with([
            'Sapkeys.Clients:id,name',
            'Sapkeys:id,key_sap,clients_id'
        ])->where('users_id', $user)->paginate($perPage);
        return response()->json($orders, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'order' => 'required|unique:orders,order',
                'typeDelivery' => 'required',
                'date' => 'required',
                'idUser' => 'required',
                'idClients' => 'required',
                'Process' => 'required|in:Ventas,Compras,EntregaSurtido,Empaque,Facturacion,Logistica',
                'Ruta' => 'nullable',
                'Envio' => 'nullable',
                'Terminado' => 'nullable',
                'status' => 'required|in:Activa,Pendiente,Cerrada',
            ]);

            $order = new Orders();
            $order->order = $validatedData['order'];
            $order->typedelivery = $validatedData['typeDelivery'];
            $order->date = date('Y-m-d H:i:s', strtotime($validatedData['date']));
            $order->users_id = $validatedData['idUser'];
            $order->sap_keys_id = $validatedData['idClients'];
            $order->process = $validatedData['Process'];
            $order->status = $validatedData['status'];
            $order->save();

            $time = new Time();
            $time->department = $validatedData['Process'];
            $time->orders_id = $order->id;
            $time->save();

            return response()->json($order, Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function show($order)
    {
        $orderV = Orders::with('SapKeys.Clients')->where('order', $order)->first();

        if (!$orderV) {
            return response()->json($orderV, Response::HTTP_NOT_FOUND);
        }
        return response()->json($orderV, Response::HTTP_OK);
    }

    public function showOrder(Orders $orders)
    {
        if (!$orders) {
            return response()->json([
                'message' => 'No se encontro la orders',
            ]);
        }

        return response()->json($orders, Response::HTTP_OK);
    }

    public function showTranfers(Request $request)
    {
        $perPage = $request->per_pages;
        $search = $request->search ?? '';

        $query = Orders::with('SapKeys.Clients', 'Details')
            ->whereHas('Details', function ($query) {
                $query->where('types', 'Traspaso');
            });

        if (!empty($search)) {
            $query->where(function ($q) use ($search) {
                $q->where('order', 'LIKE', "%$search%")
                    ->orWhereHas('SapKeys.Clients', function ($subQuery) use ($search) {
                        $subQuery->where('name', 'LIKE', "%$search%");
                    });
            });
        }

        $orders = $query->paginate($perPage);

        return response()->json($orders, Response::HTTP_OK);
    }

    public function showDelivery(Request $request)
    {
        $perPage = $request->input('per_pages', 10);
        $search = $request->input('search', '');

        $orders = Orders::with([
            'Sapkeys.Clients:id,name',
            'Sapkeys:id,key_sap,clients_id'
        ])
            ->where(function ($query) {
                $query->whereIn('process', ['Ventas', 'Compras', 'EntregaSurtido']);
            })
            ->whereNull('deleted_at')
            ->whereDoesntHave('Details', function ($detailsQuery) {
                $detailsQuery->where('types', 'Traspaso');
            });
        if (!empty($search)) {
            $orders->where(function ($q) use ($search) {
                $q->where('order', 'LIKE', "%$search%")
                    ->orWhereHas('SapKeys.Clients', function ($subQuery) use ($search) {
                        $subQuery->where('name', 'LIKE', "%$search%");
                    });
            });
        }

        $orderResults = $orders->paginate($perPage);

        return response()->json($orderResults, Response::HTTP_OK);
    }


    public function showOrderInvoice(Request $request)
    {
        $perPage = $request->per_page;

        $query = Orders::with("labels.unions", "sapKeys.clients")
            ->where(function ($query) {
                $query->whereIn('process', ['Empaque', 'Facturacion']);
            })
            ->whereDoesntHave('labels.unions');

        $unrelatedOrders = $query->paginate($perPage);

        if ($unrelatedOrders->isEmpty()) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }

        return response()->json($unrelatedOrders, Response::HTTP_OK);
    }
    public function update(Request $request, Orders $orders)
    {
        try {
            $request->validate([
                'order' => 'required|unique:orders,order',
                'typeDelivery' => 'required',
                'date' => 'required',
                'idUser' => 'required',
                'idClients' => 'required',
                'Process' => 'required|in:Ventas,Compras,EntregaSurtido,Empaque,Facturacion,Logistica',
                'Ruta',
                'Envio',
                'Terminado',
                'status' => 'required|in:Activa,Pendiente,Cerrada',
            ]);

            $orders->order = $request->order;
            $orders->typedelivery = $request->typeDelivery;
            $orders->date = date('Y-m-d H:i:s', strtotime($request->date));
            $orders->users_id = $request->idUser;
            $orders->sap_keys_id = $request->idClients;
            $orders->process = $request->Process;
            $orders->status = $request->status;
            $orders->save();
            return response()->json($orders, Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function Process(Request $request, Orders $orders)
    {
        $request->validate([
            'process' => 'required|in:Ventas,Compras,EntregaSurtido,Empaque,Facturacion,Logistica'
        ]);

        $orders->process = $request->process;
        $idLabels = $orders->labels->pluck('id');
        $unions = Unions::whereIn('label_id', $idLabels)->get();
        // if (!empty($unions)) {
        foreach ($unions as $union) {
            $relatedOrders = $union->labels->Orders->pluck('id');
            Orders::whereIn('id', $relatedOrders)->update(['process' => $request->process]);
        }
        $orders->save();
        $time = Time::where('orders_id', $orders->id)->first();
        // if ($time) {
        //     $time->department = $request->process;
        //     $time->save();
        // // }

        $time = new Time();
        $time->department = $request->process;
        $time->orders_id = $orders->id;
        $time->save();

        return response()->json($orders, Response::HTTP_ACCEPTED);
    }

    public function destroy(Orders $orders)
    {
        $orders->delete();
        return response()->json($orders, Response::HTTP_NO_CONTENT);
    }
}