<?php

namespace App\Http\Controllers;

use App\Events\CommentCreated;
use App\Notifications\NewCommentNotification;
use Illuminate\Http\Request;
use App\Models\Comments;
use App\Models\User;
use Illuminate\Http\Response;

class CommentsController extends Controller
{
    public function index()
    {
        $user = auth()->user()->id;
        $comment = Comments::with('Orders')->where('users_id', $user)->get();
        return response()->json($comment, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            "users_id" => "required",
            'orders_id' => 'required',
            'comment' => 'required',
            'department' => 'required|in:All,Ventas,Compras,Surtido,Empaque',
        ]);

        $comment = new Comments;
        $comment->user_id = $request->users_id;
        $comment->orders_id = $request->orders_id;
        $comment->comment = $request->comment;
        $comment->department = $request->department;
        $comment->save();

        $usersWithSameOrder = User::whereHas('Comment', function ($query) use ($request) {
            $query->where('orders_id', $request->orders_id);
        })->get();

        foreach ($usersWithSameOrder as $user) {
            $comment = Comments::find($comment->id);
            $user->notify(new NewCommentNotification($comment));
        }
        event(new CommentCreated($comment));

        return response()->json($comment, Response::HTTP_CREATED);
    }

    public function show($orders_id)
    {
        $comment = Comments::with('users')->where('orders_id', $orders_id)->get();

        return response()->json($comment);
    }

}