<?php

namespace App\Http\Controllers;

use App\Models\Labels;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Search extends Controller
{
    public function search(Request $request)
    {
        $text = $request->termin;
        $order = Orders::search($text)->get();
        $labes = Labels::search($text)->get();
        $result = null;
        // if ($order) {
        //     $result = $order;
        // } else if ($labes) {
        //     $result = $labes;
        // }
        $result = [
            "order" => $order,
            "labels" => $labes
        ];

        return response()->json($result, Response::HTTP_OK);
    }
}