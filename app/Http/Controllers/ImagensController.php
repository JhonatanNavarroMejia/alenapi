<?php

namespace App\Http\Controllers;

use App\Models\Labels;
use App\Models\Imagens;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ImagensController extends Controller
{
    public function index()
    {
        $imagen = Imagens::all();
        return response()->json(['files' => $imagen]);
    }

    public function store(Request $request)
    {
        if ($request->hasFile('files')) {
            $uploadedImages = [];
            $label = Labels::find($request->labels);
            if ($label) {
                $labelDelivery = $label->delivery_order;

                foreach ($request->file('files') as $file) {
                    if ($file->isValid()) {
                        $filename = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $newName = $labelDelivery . '_' . $filename;
                        $path = 'public/Images/' . $newName;
                        $image = Image::make($file);
                        $image->encode('png', 18);
                        Storage::disk('local')->put($path, $image->getEncoded());

                        if (Storage::disk('local')->exists($path)) {
                            $imagen = new Imagens();
                            $imagen->name = $newName;
                            $imagen->labels_id = $request->labels;

                            if ($imagen->save()) {
                                $uploadedImages[] = $newName;
                            } else {
                                $this->deleteImage($path);
                            }
                        } else {
                            $this->deleteImage($path);
                        }
                    }
                }
            } else {
                return response()->json('Etiqueta no encontrada', Response::HTTP_BAD_REQUEST);
            }

            if (!empty($uploadedImages)) {
                return response()->json('Se subieron las imágenes', Response::HTTP_CREATED);
            } else {
                return response()->json("No se pudo guardar las imágenes", Response::HTTP_BAD_REQUEST);
            }
        }
    }


    private function deleteImage($path)
    {
        Storage::disk('images')->delete($path);
    }


    public function show($labels_id)
    {
        $img = Imagens::where('labels_id', $labels_id)->get();
        return response()->json($img, Response::HTTP_OK);
    }

    public function showP($filepath)
    {
        $path = 'Images/' . $filepath;
        if (Storage::disk('Images')->exists($filepath)) {
            return response()->file(storage_path('app/public/' . $path));
        } else {
            return response()->json("No se encontro imagen", Response::HTTP_NOT_FOUND);
        }
    }

    public function destroy(Imagens $imagen)
    {
        $path = 'public/Images/' . $imagen->name;
        Storage::delete($path);
        $imagen->delete();
        return response()->json($imagen, Response::HTTP_OK);
    }
}