<?php

namespace App\Http\Controllers;

use App\Mail\UserEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;
use Spatie\Permission\Traits\HasRoles;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'user' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'location' => 'required|in:GDL,CDMX,TJN,VER,TX,PANA',
            'departament' => 'required|in:All,Ventas,Compras,Entregas,Entradas,Empaque,Facturacion,Logistica',
            'idType' => 'required',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->user = $request->user;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->location = $request->location;
        $user->departament = $request->departament;
        $user->save();

        // Asignar roles y permisos basados en la combinación de departamento y tipo
        if ($request->idType === '3') {
            if ($request->departament === 'Ventas') {
                $user->assignRole('CoordinadorV');
            } elseif ($request->departament === 'Compras') {
                $user->assignRole('CoordinadorC');
            } elseif ($request->departament === 'Entregas' || $request->departament === 'Empaque') {
                $user->assignRole('CoordinadorA');
            }
        } else {
            $user->assignRole($request->idType);
        }

        Mail::to($user->email)->send(new UserEmail($request));

        return response($user, Response::HTTP_CREATED);
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'user' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('token')->plainTextToken;
            $cookie = cookie('cookie_token', $token, 60 * 24);

            return response(['token' => $token], Response::HTTP_OK)->withCookie($cookie);
        }

        return response(["message" => "Credeciales invalidas"], Response::HTTP_UNAUTHORIZED);
    }

    public function userProfile(Request $request)
    {
        $user = auth()->user();
        $data = [
            "message" => "userProfile OK",
            "user" => $user,
            "rol" => $user->getRoleNames(),
            "permissions" => $user->getPermissionsViaRoles(),
        ];
        return response()->json($data, Response::HTTP_OK);
    }

    public function logout()
    {
        $cookie = Cookie::forget('cookie_token');
        return response(["message" => "Se cerro la sessin OK"], Response::HTTP_OK)->withCookie($cookie);
    }

    public function index(Request $request)
    {
        $perPage = $request->per_pages;
        $user = User::paginate($perPage);
        return Response()->json($user, Response::HTTP_OK);
    }

    public function update(Request $request, User $user)
    {
        try {
            $request->validate([
                'name' => 'required',
                'user' => 'required',
                'email' => 'required',
                'password' => 'required',
                'location' => 'required|in:GDL,CDMX,TIJ,VER,TX,PANA',
                'departament' => 'required|in:All,Ventas,Compras,Surtido,Empaque'
            ]);

            $user->name = $request->name;
            $user->user = $request->user;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->location = $request->location;
            $user->departament = $request->departament;
            $user->save();

            return response()->json($user, Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(User $user)
    {
        if (Auth::user()->id === $user->id) {
            return response()->json(['error' => 'No tienes autorización para eliminar este usuario.'], Response::HTTP_FORBIDDEN);
        }

        $user->delete();
        return response()->json($user, Response::HTTP_NO_CONTENT);
    }
}