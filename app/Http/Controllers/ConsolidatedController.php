<?php

namespace App\Http\Controllers;

use App\Models\consolidated;
use App\Models\Labels;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Resources\consolidatedResource;

class ConsolidatedController extends Controller
{
    public function index(Labels $labels)
    {
        $labelsSon = consolidated::with('labels')->where('label', $labels->id)->get();
        return response()->json($labelsSon, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            "Store" => "required",
        ]);

        $consolidation = new consolidated;
        $consolidation->store = $request->Store;
        $consolidation->save();
        return response()->json($consolidation, Response::HTTP_CREATED);
    }
}