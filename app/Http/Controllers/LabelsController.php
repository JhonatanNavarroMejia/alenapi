<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Labels;
use App\Models\Orders;
use App\Models\Unions;
use Illuminate\Http\Request;
use App\Models\consolidated;
use Illuminate\Http\Response;
use App\Http\Resources\LabelsResource;
use App\Models\Packing_Details;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Empty_;

class LabelsController extends Controller
{
    public function index(Request $request)
    {
        $perPage = $request->per_pages;
        $search = $request->search ?? '';
        $user = auth()->user()->location;

        $query = Labels::with([
            'Users:id,location',
            'packing_details',
            'Orders:id,sap_keys_id,typeDelivery,order,process',
            'Orders.sapkeys:id,clients_id',
            'Orders.Sapkeys.Clients:id,name',
        ])
            ->orderBy('status', 'asc')
            ->whereHas('Users', function ($q) use ($user) {
                $q->where('location', $user);
            })
            ->whereHas('Orders', function ($q) {
                $q->whereIn('process', ['EntregaSurtido', 'Empaque']);
            })
            ->whereNotIn('id', Unions::pluck('labels_id')->toArray())
            ->when($search, function ($q) use ($search) {
                $q->where(function ($innerQ) use ($search) {
                    $innerQ->where('label', 'like', '%' . $search . '%')
                        ->orWhereHas('Orders.Sapkeys.Clients', function ($query) use ($search) {
                            $query->where('name', 'like', '%' . $search . '%');
                        });
                });
            });

        $labels = $query->paginate($perPage);

        $labelsWithConsolidated = $labels->filter(function ($label) {
            return !$label->consolidated || !$this->consolidated->contains($label->consolidated->id);
        });

        $this->consolidated = $labelsWithConsolidated->pluck('consolidated_id');

        $labelsResource = LabelsResource::collection($labelsWithConsolidated);

        return response()->json($labelsResource, Response::HTTP_OK);
    }

    public function showLabel($orders_id)
    {
        $labels = Labels::with("Orders")->get();

        return response()->json($labels, Response::HTTP_OK);
    }

    public function indexLabel()
    {
        $unions = Unions::pluck('labels_id')->toArray();
        $consoli = consolidated::pluck('id')->toArray();

        $query = Labels::with(
            [
                "Users:id,location",
                "packing_details",
                'Orders:id,sap_keys_id,typeDelivery',
                'Orders.Sapkeys.Clients:id,name',
                'Orders.Sapkeys:id,key_sap,clients_id'
            ]
        )->orderBy("status", "asc");
        $label = $query->whereNotIn('id', $unions)->orWhereNull('id')->whereNotIn('consolidated_id', $consoli)->orWhereNull('consolidated_id')->get();
        return response()->json($label, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            "label" => 'required|unique:labels,label',
            "delivery_order" => "required",
            "split" => "required",
            "orders_id" => "required",
            "users_id" => "required",
            "status" => "required|in:Activo,Pendiente,Cerrado"
        ]);

        $labels = new Labels;
        $labels->label = $request->label;
        $labels->delivery_order = $request->delivery_order;
        $labels->split = $request->split;
        $labels->orders_id = $request->orders_id;
        $labels->users_id = $request->users_id;
        $labels->status = $request->status;
        $labels->save();
        return response()->json($labels, Response::HTTP_OK);
    }

    public function storeFile(Request $request, Labels $labels)
    {
        $request->validate([
            "file" => 'required|mimes:docx,pdf|max:2048',
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            if ($file->isValid()) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $timestamp = date('Y-m-d');
                $newFilename = $request->invoice_folio . '_' . $filename;
                $path = 'public/Files/' . $newFilename;

                $file->storeAs('', $path, 'local');

                if (Storage::disk('local')->exists($path)) {
                    $labels->file_invoice = $newFilename;
                    $labels->invoice_folio = $request->invoice_folio;
                    $labels->save();
                }
            }
            return response()->json("Archivo agregado", Response::HTTP_OK);
        } else {
            return response()->json("No se encontro ningun archivo", Response::HTTP_BAD_REQUEST);
        }
    }
    public function show($orders_id)
    {
        $order = Orders::with("Labels", "Sapkeys.Clients:id,name", 'Sapkeys:id,key_sap,clients_id')->where("id", $orders_id)->first();

        return response()->json($order, Response::HTTP_OK);
    }

    public function showLabelImg($labels)
    {
        $label = Labels::with('Imagens')->find($labels);

        $labelsIds = Unions::where('label_id', $label->id)->pluck('labels_id');
        // dd($labelsIds);

        $relatedImages = Labels::with('Imagens')
            ->whereIn('id', $labelsIds)
            ->get()
            ->pluck('Imagens')
            ->flatten();

        return response()->json($label, Response::HTTP_OK);
    }

    public function showLast()
    {
        $last = Labels::select("label")->orderBy('label', 'desc')->first();
        return response()->json($last, Response::HTTP_ACCEPTED);
    }

    public function process(Request $request, Labels $labels)
    {
        $request->validate([
            'status' => "required|in:Activo,Pendiente,Cerrado",
        ]);
        $labels->status = $request->status;
        $labels->save();
        return response()->json($labels, Response::HTTP_OK);
    }

    public function destroy(Labels $labels)
    {
        $labels->delete();
        return response()->json($labels, Response::HTTP_NO_CONTENT);
    }

    public function generatePDF(Labels $labels)
    {
        $currentDate = now();
        $name = $labels->orders->SapKeys->Clients;
        $totalCajas = 0;
        $totalRollos = 0;
        $totalCarrete = 0;
        $totalTarima = 0;
        $totalPaquete = 0;
        $totalOtros = 0;
        $totalOtros1 = 0;
        $associatedLabels = [];
        $packingUnions = null;

        $labelsUnions = Unions::where("label_id", $labels->id)->get();
        if ($labelsUnions->isNotEmpty()) {
            $associatedLabelIds = $labelsUnions->pluck('labels_id')->toArray();
            $associatedLabels = Labels::whereIn('id', $associatedLabelIds)->get();
            $associatedLabelIds[] = $labels->id;
            $associatedLabels[] = $labels;
            $packingUnions = Packing_Details::whereIn('labels_id', $associatedLabelIds)->get();
            foreach ($packingUnions as $packing) {
                $totalCajas += $packing->box ?? 0;
                $totalRollos += $packing->roll ?? 0;
                $totalCarrete += $packing->rell ?? 0;
                $totalTarima += $packing->pallet ?? 0;
                $totalPaquete += $packing->pack ?? 0;
                $totalOtros += $packing->option ?? 0;
                $totalOtros1 += $packing->option1 ?? 0;
            }
        }

        $packingDetails = null;
        if ($labels->consolidated_id !== null) {
            $consolidatedLabels = Consolidated::where("id", $labels->consolidated_id)->get();
            if ($consolidatedLabels->isNotEmpty()) {
                $consolidatedLabelIds = $consolidatedLabels->pluck('label_id')->toArray();
                $packingDetails = Packing_Details::whereIn('labels_id', $consolidatedLabelIds)->first();
                $totalCajas = ($packingDetails ? $packingDetails->box : 0);
                $totalRollos = ($packingDetails ? $packingDetails->roll : 0);
                $totalCarrete = ($packingDetails ? $packingDetails->rell : 0);
                $totalTarima = ($packingDetails ? $packingDetails->pallet : 0);
                $totalPaquete = ($packingDetails ? $packingDetails->pack : 0);
                $totalOtros = ($packingDetails ? $packingDetails->option : 0);
                $totalOtros1 = ($packingDetails ? $packingDetails->option1 : 0);
            }
        }

        if (empty($associatedLabels)) {
            $packingDetails = Packing_Details::where('labels_id', $labels->id)->first();
            $totalCajas = ($packingDetails ? $packingDetails->box : 0);
            $totalRollos = ($packingDetails ? $packingDetails->roll : 0);
            $totalCarrete = ($packingDetails ? $packingDetails->rell : 0);
            $totalTarima = ($packingDetails ? $packingDetails->pallet : 0);
            $totalPaquete = ($packingDetails ? $packingDetails->pack : 0);
            $totalOtros = ($packingDetails ? $packingDetails->option : 0);
            $totalOtros1 = ($packingDetails ? $packingDetails->option1 : 0);
        }

        $totalAll = $totalCajas + $totalCarrete + $totalOtros + $totalOtros1 + $totalPaquete + $totalPaquete + $totalRollos + $totalTarima;

        $data = [
            'label' => $labels,
            'client' => $name,
            'date' => $currentDate,
            'labels' => !empty($associatedLabels) ? $associatedLabels : [$labels],
            'totalCajas' => $totalCajas,
            'totalPaquetes' => $totalPaquete,
            'totalRollos' => $totalRollos,
            'totalCarrete' => $totalCarrete,
            'totalTarima' => $totalTarima,
            'Otros1' => ($packingDetails ? $packingDetails->optionDatalist : "OTROS"),
            'totalOtros' => $totalOtros,
            'Otros2' => ($packingDetails ? $packingDetails->optionDatalist1 : "OTROS"),
            'TotalOtros1' => $totalOtros1,
            'totalAll' => $totalAll,
        ];
        $pdf = Pdf::loadView("pdf.labelPDF", $data);
        return $pdf->download('product_details.pdf');
    }

    public function updateLink(Request $request, Labels $labels)
    {
        $labels->consolidated_id = $request->consolidated_id;
        $labels->save();
        return response()->json($labels, Response::HTTP_OK);
    }
}