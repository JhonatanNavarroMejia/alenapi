<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\sap_keys;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ClientsController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->search;
        $query = Clients::with("SapKeys");
        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', "%$search%")
                    ->orWhere('rfc', 'LIKE', "%$search%");
            });
        }

        $clients = $query->get();
        return response()->json($clients, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|unique:clients,name',
                'rfc' => 'required|unique:clients,rfc',
                'compania' => 'required|in:Automatización,Electrica,Todas',
                'keysaps' => 'required|unique:sap_keys,key_sap'
            ], [
                'name.unique' => 'el nombre ya existe',
                'rfc.unique' => 'El rfc ya existe',
                'key_sap.unique' => 'La clave ya se existe',
            ]);

            $client = new Clients;
            $client->name = $request->name;
            $client->rfc = $request->rfc;
            $client->compania = $request->compania;
            $client->save();

            $keys = new sap_keys;
            $keys->key_sap = $request->keysaps;
            $keys->clients_id = $client->id;
            $keys->save();
            // dd($keys);

            return response()->json($client, Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function show(Clients $clients)
    {
        if (!$clients) {
            $client = $clients::with("SapKeys")->get();
            return response()->json($client, Response::HTTP_NOT_FOUND);
        }
        return response()->json($clients, Response::HTTP_FOUND);
    }

    public function update(Request $request, Clients $clients)
    {
        try {
            $this->validate($request, [
                'name' => 'required|unique:clients,name',
                'rfc' => 'required|unique:clients,rfc',
                'compania' => 'required|in:Automatización,Electrica,Todas',
            ], [
                'name.unique' => 'el nombre ya existe',
                'rfc.unique' => 'El rfc ya existe',
            ]);
            $clients->name = $request->name;
            $clients->rfc = $request->rfc;
            $clients->compania = $request->compania;
            $clients->save();

            return response()->json($clients, Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(Clients $clients)
    {
        $clients->delete();

        return response()->json($clients, Response::HTTP_NO_CONTENT);
    }
}