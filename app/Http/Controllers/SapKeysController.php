<?php

namespace App\Http\Controllers;

use App\Models\sap_keys;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SapKeysController extends Controller
{
    public function index()
    {
        $sap_keys = sap_keys::all();
        return response()->json($sap_keys, Response::HTTP_OK);
    }
    public function store(Request $request)
    {
        try {
            $request->validate([
                "key_sap" => 'required|unique:sap_keys,key_sap',
                'clients_id' => 'required',
            ], [
                'key_sap.unique' => 'La clave ya existe',
            ]);

            $sapkeys = new sap_keys();
            $sapkeys->key_sap = $request->key_sap;
            $sapkeys->clients_id = $request->clients_id;
            $sapkeys->save();
            return response($sapkeys, Response::HTTP_CREATED);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function show(sap_keys $sap_keys)
    {
        if (!$sap_keys) {
            return response()->json($sap_keys, Response::HTTP_NOT_FOUND);
        }
        return response()->json($sap_keys, Response::HTTP_FOUND);
    }

    public function update(Request $request, sap_keys $sap_keys)
    {
        try {
            $request->validate([
                "key_sap" => 'required|unique:sap_keys,key_sap',
                'clients_id' => 'required',
            ], [
                'key_sap.unique' => 'La clave ya existe',
            ]);

            $sap_keys->key_sap = $request->key_sap;
            $sap_keys->clients_id = $request->clients_id;
            $sap_keys->save();
            return response($sap_keys, Response::HTTP_OK);
        } catch (ValidationException $e) {
            return response()->json($e->errors(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function destroy(sap_keys $sap_keys)
    {
        $sap_keys->delete();
        return response()->json($sap_keys, Response::HTTP_NO_CONTENT);
    }
}