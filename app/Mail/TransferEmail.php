<?php

namespace App\Mail;

use App\Models\Details;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class TransferEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    public $orders; // Datos del padre

    /**
     * Create a new message instance.
     */
    public function __construct(Details $details)
    {
        $this->details = $details;
        $this->orders = $details->Orders;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Ordenes con traspasos',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.transfer', // Asegúrate de tener una vista llamada "transfer.blade.php"
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}