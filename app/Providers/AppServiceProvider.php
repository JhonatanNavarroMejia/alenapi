<?php

namespace App\Providers;

use App\Models\Details;
use App\Models\User;
use App\Observers\DetailObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Details::observe(DetailObserver::class);
        User::observe(UserObserver::class);
    }
}