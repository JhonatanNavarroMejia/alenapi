<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Labels', function (Blueprint $table) {
            $table->id();
            $table->integer('label');
            $table->string("delivery_order");
            $table->string("split");
            $table->string("invoice_folio")->nullable();
            $table->string("file_invoice")->nullable();
            $table->enum('process', ['Entrega', 'Entrada', 'Empaque', 'Facturacion', 'Logistica', 'Ruta', 'Envio', 'Terminado']);
            $table->enum('status', ["Activo", 'Pendiente', 'Cerrado']);
            $table->foreignId("orders_id")->constrained("Orders");
            $table->foreignId("users_id")->constrained("Users");
            $table->foreignId('consolidated_id')->nullable()->constrained('consolidateds');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('labels');
    }
};