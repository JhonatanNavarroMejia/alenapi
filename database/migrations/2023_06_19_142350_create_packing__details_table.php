<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Packing_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('box');
            $table->unsignedInteger('pallet');
            $table->unsignedInteger('roll');
            $table->unsignedInteger('rell');
            $table->unsignedInteger('pack');
            $table->string('optionDatalist');
            $table->unsignedInteger('option');
            $table->string('optionDatalist1');
            $table->unsignedInteger('option1');
            $table->text('comment')->nullable();
            $table->foreignId('labels_id')->constrained('Labels');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('packing__details');
    }
};