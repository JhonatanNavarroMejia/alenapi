<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Details', function (Blueprint $table) {
            $table->id();
            $table->enum('types', ['Normal', 'Traspaso']);
            $table->string("detail_split", 50);
            $table->enum('companyO', ['Automatizacion', 'Intelligent']);
            $table->enum('storehouseStart', ['01', '02', '03', '04']);
            $table->enum('companyD', ['Automatizacion', 'Intelligent']);
            $table->enum('storehouseEnd', ['01', '02', '03', '04']);
            $table->foreignId("orders_id")->constrained('Orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('details');
    }
};