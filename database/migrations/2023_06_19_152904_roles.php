<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $order_views = Permission::create(['name' => 'order.views']);
        $order_create = Permission::create(['name' => 'order.create']);
        $order_read = Permission::create(['name' => 'order.read']);
        $order_update = Permission::create(['name' => 'order.update']);
        $order_delete = Permission::create(['name' => 'order.delete']);

        $transfers_views = Permission::create(['name' => 'transfers.views']);
        $transfers_create = Permission::create(['name' => 'transfers.create']);
        $transfers_read = Permission::create(['name' => 'transfers.read']);
        $transfers_update = Permission::create(['name' => 'transfers.update']);
        $transfers_delete = Permission::create(['name' => 'transfers.delete']);

        $delivery_views = Permission::create(['name' => 'deliverys.views']);
        $delivery_create = Permission::create(['name' => 'deliverys.create']);
        $delivery_read = Permission::create(['name' => 'deliverys.read']);
        $delivery_update = Permission::create(['name' => 'deliverys.update']);
        $delivery_delete = Permission::create(['name' => 'deliverys.delete']);

        $paking_views = Permission::create(['name' => 'paking.views']);
        $paking_create = Permission::create(['name' => 'paking.create']);
        $paking_read = Permission::create(['name' => 'paking.read']);
        $paking_update = Permission::create(['name' => 'paking.update']);
        $paking_delete = Permission::create(['name' => 'paking.delete']);

        $invoicing_views = Permission::create(['name' => 'invoicing.views']);
        $invoicing_create = Permission::create(['name' => 'invoicing.create']);
        $invoicing_read = Permission::create(['name' => 'invoicing.read']);
        $invoicing_update = Permission::create(['name' => 'invoicing.update']);
        $invoicing_delete = Permission::create(['name' => 'invoicing.delete']);

        $quality_views = Permission::create(['name' => 'quality.views']);
        $quality_create = Permission::create(['name' => 'quality.create']);
        $quality_read = Permission::create(['name' => 'quality.read']);
        $quality_update = Permission::create(['name' => 'quality.update']);
        $quality_delete = Permission::create(['name' => 'quality.delete']);

        $report_views = Permission::create(['name' => 'report.views']);
        $report_create = Permission::create(['name' => 'report.create']);
        $report_read = Permission::create(['name' => 'report.read']);
        $report_update = Permission::create(['name' => 'report.update']);
        $report_delete = Permission::create(['name' => 'report.delete']);

        $client_views = Permission::create(['name' => 'client.views']);
        $client_create = Permission::create(['name' => 'client.create']);
        $client_read = Permission::create(['name' => 'client.read']);
        $client_update = Permission::create(['name' => 'client.update']);
        $client_delete = Permission::create(['name' => 'client.delete']);

        $user_views = Permission::create(['name' => 'user.views']);
        $user_create = Permission::create(['name' => 'user.create']);
        $user_read = Permission::create(['name' => 'user.read']);
        $user_update = Permission::create(['name' => 'user.update']);
        $user_delete = Permission::create(['name' => 'user.delete']);

        $direction_views = Permission::create(['name' => 'direction.views']);
        $direction_create = Permission::create(['name' => 'direction.create']);
        $direction_read = Permission::create(['name' => 'direction.read']);
        $direction_update = Permission::create(['name' => 'direction.update']);
        $direction_delete = Permission::create(['name' => 'direction.delete']);

        $label_views = Permission::create(['name' => 'label.views']);
        $label_create = Permission::create(['name' => 'label.create']);
        $label_read = Permission::create(['name' => 'label.read']);
        $label_update = Permission::create(['name' => 'label.update']);
        $label_delete = Permission::create(['name' => 'label.delete']);


        $Splits_views = Permission::create(['name' => 'splits.views']);
        $splits_create = Permission::create(['name' => 'splits.create']);
        $Splits_read = Permission::create(['name' => 'splits.read']);
        $Splits_update = Permission::create(['name' => 'splits.update']);
        $Splits_delete = Permission::create(['name' => 'splits.delete']);

        $roleAdmin = Role::create(['name' => 'Admin']);
        $roleAdmin->givePermissionTo(Permission::all());

        $roleCoV = Role::create(['name' => 'CoordinadorV']);
        $roleCoV->givePermissionTo([
            $order_views,
            $client_views,
            $user_views,
            $user_create
        ]);

        $roleVendedor = Role::create(['name' => 'Ventas']);
        $roleVendedor->givePermissionTo([
            $order_views,
            $order_create,
            $order_read,
            $order_update,
            $order_delete
        ]);
        $roleCoC = Role::create(['name' => 'CoordinadorC']);
        $roleCoC->givePermissionTo([
            $transfers_views,
            $user_views,
            $user_create
        ]);

        $roleCompras = Role::create(['name' => 'Compras']);
        $roleCompras->givePermissionTo([
            $transfers_views,
            $transfers_create,
            $transfers_read,
            $transfers_update,
            $transfers_delete
        ]);
        $roleCoA = Role::create(['name' => 'CoordinadorA']);
        $roleCoA->givePermissionTo([
            $delivery_views,
            $paking_views,
            $invoicing_views,
            $client_views,
            $user_views,
            $user_create
        ]);

        $roleSurtidos = Role::create(['name' => 'Surtidos']);
        $roleSurtidos->givePermissionTo([
            $delivery_views,
            $delivery_create,
            $delivery_read,
            $delivery_update,
            $delivery_delete,
        ]);
        $roleEmpaque = Role::create(['name' => 'Empaque']);
        $roleEmpaque->givePermissionTo([
            $paking_views,
            $paking_create,
            $paking_read,
            $paking_update,
            $paking_delete,
        ]);
        $roleFacturacion = Role::create(['name' => 'Facturacion']);
        $roleFacturacion->givePermissionTo([
            $invoicing_views,
            $invoicing_create,
            $invoicing_read,
            $invoicing_update,
            $invoicing_delete
        ]);
        $roleLogistica = Role::create(['name' => 'Logistica']);
        $roleLogistica->givePermissionTo([

        ]);
        $roleCalidad = Role::create(['name' => 'Calidad']);
        $roleCalidad->givePermissionTo([
            $quality_views,
            $quality_create,
            $quality_read,
            $quality_update,
            $quality_delete
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};