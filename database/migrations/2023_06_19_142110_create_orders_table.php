<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Orders', function (Blueprint $table) {
            $table->id();
            $table->integer("order")->unsigned();
            $table->enum('typeDelivery', ['Normal', 'Inmediata', 'El cliente pasa']);
            $table->dateTime('date')->format('YYYY-MM-DDTHH:MI:SS');
            $table->foreignId("users_id")->constrained("Users");
            $table->foreignId("sap_keys_id")->constrained("sap_keys");
            $table->enum('process', ['Ventas', 'Compras', 'EntregaSurtido', 'Empaque', 'Facturacion', 'Logistica', 'Ruta', 'Envio', 'Terminado']);
            $table->enum("status", ['Activa', 'Pendiente', 'Cerrada']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};