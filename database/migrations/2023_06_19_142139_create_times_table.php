<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Times', function (Blueprint $table) {
            $table->id();
            $table->enum("department", ['Ventas', 'Compras', 'EntregaSurtido', 'Empaque', 'Facturacion']);
            $table->foreignId('orders_id')->constrained('Orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('times');
    }
};