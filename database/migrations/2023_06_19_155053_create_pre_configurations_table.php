<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pre_configurations', function (Blueprint $table) {
            $table->id();
            $table->enum('typeCharters', ['A Domicilio', 'Ocurre']);
            $table->enum('methods', ['Cob. Reg', 'Credito', 'Pagado', 'X cobrar']);
            $table->foreignId('idDirection')->constrained('Directions');
            $table->enum('typeDocument', ['Directo', 'Reembarque', 'Ruta']);
            $table->enum('location', ['GDL', 'CDMX', 'TJN', 'VER', 'TX', 'PAN']);
            $table->enum("parcelOffices", ['GDL', 'CDMX', 'TJN', 'VER', 'TX', 'PAN']);
            $table->foreignId('idDriver')->constrained('Users');
            $table->foreignId('clients_id')->constrained('Clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pre_configurations');
    }
};