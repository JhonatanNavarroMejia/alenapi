<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('freights', function (Blueprint $table) {
            $table->id();
            $table->string("content", 40);
            $table->foreignId('directions_id')->constrained("directions");
            $table->float("direct_cost");
            $table->enum('type_document', ['Directo', 'Reembarque', 'Ruta']);
            $table->string("observation");
            $table->date("date_start");
            $table->float("reshipment_cost");
            $table->string("direct_guide");
            $table->string("reshipment_guide");
            $table->enum('branch', ['GDL', 'VER', 'TJN', 'TX', 'MIA', 'CDMX']);
            $table->date("date_end");
            $table->enum('type_freights', ['OCURRE', 'A DOMICILIO']);
            $table->foreignId("parcel_id")->constrained("parcels");
            $table->enum('payment_method', ['X COBRAR', 'PAGADO', 'CREDITO', 'COB. REG']);
            $table->foreignId('id_driver')->constrained("Users");
            $table->enum('status', ['Ventas', 'Ruta', 'Envios', 'Terminado']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('freights');
    }
};