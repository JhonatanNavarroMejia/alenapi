<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('consolidateds', function (Blueprint $table) {
            $table->id();
            // $table->foreignId('label')->constrained("Labels");
            $table->enum('Store', ['GDL', 'TJN', 'CDMX', 'VER']);
            // $table->foreignId('label_id')->constrained("Labels");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('consolidateds');
    }
};