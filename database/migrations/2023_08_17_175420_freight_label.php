<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('freight_label', function (Blueprint $table) {
            $table->foreignId('freight_id')->constrained('freights'); // Clave externa a "freights"
            $table->foreignId('label_id')->constrained('labels'); // Clave externa a "labels"
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};