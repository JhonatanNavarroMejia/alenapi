<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('Directions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client')->constrained('Clients');
            $table->string('street');
            $table->string('suburb');
            $table->string('town');
            $table->string('state');
            $table->string('postalCode');
            $table->string('phone');
            $table->string('nameDirection');
            $table->string('referenceDirection');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('directions');
    }
};